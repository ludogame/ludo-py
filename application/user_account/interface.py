import click
from application.sign_up_account.dto.sign_up_dto import SignUpDto
from application.share.utils import utils, print_all
from application.share.security import secure
from application.share.api_constraint import ApiGroup
from app import app, user_cli_group

from marshmallow import Schema, fields

class AccountDto(Schema):
    username = fields.Str(required=True)
    password = fields.Str(required=True)

from .service import service

def _dbg(param):
    print(param)

@app.route(f"{ApiGroup.USER.value}/sign-up", methods=["POST"])
# @secure.user_auth
@utils.request_with_body
@utils.validate(AccountDto)
def sign_up(data):
    # _dbg("sign up api")

    return service.sign_up(data)

@app.route(f"{ApiGroup.USER.value}/sign-in", methods=["POST"])
# @secure.user_auth
@utils.request_with_body
@utils.validate(AccountDto)
def sign_in(data):
    _dbg("sign in api")
    return service.sign_in(data)

@user_cli_group.command('sign-up')
@click.option('-fb', 'facebook_url', help='Your user facebook url')
@click.option('-n', 'name', help='Your user name')
@click.option('-p', 'phone', help='Your phone number')
@click.option('-e', 'email', help='Your email address')
def sign_up(**kwargs):
    # service.sign_up(**kwargs)
    return 'hello world'