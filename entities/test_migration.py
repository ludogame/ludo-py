from datetime import datetime
from sqlalchemy.orm import relationship
from app import db
from sqlalchemy import String, DateTime, Integer
from sqlalchemy.dialects.postgresql import UUID
from uuid import uuid4

class TestMigration(db.Model):
    __tablename__ = "test_migration"

    id = db.Column(UUID(as_uuid=True), primary_key=True, default=uuid4())
    test_01 = db.Column(Integer, nullable=True)
    test_02 = db.Column(Integer, nullable=True)
