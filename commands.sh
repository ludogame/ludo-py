docker volume create ludo_db_P6a4H
docker compose up ludo-application

docker compose up

flask run --host=0.0.0.0 --port=5000


# Setup
while [[ 1 ]]
do
    rsync -a ../be/ ~/project/ludo-be/ --exclude node_modules
    sleep 0.5
    echo "=== Synced $(date) ==="
done
pushd ~/project/ludo-be/
docker compose down
docker compose up -d
docker exec -it ludo-be-ludo-application-1 bash
flask run --host=0.0.0.0 --port=5000